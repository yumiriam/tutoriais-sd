# AppEngine

1. Criar um projeto no eclipse.

2. Devemos começar com a classe que irá armazenar os dados na nuvem. E para isto precisamos usar as notações adequadas. A classe deve ter a notação "@PersistenceCapable" antes de sua declaração. Suas variáveis de instancia por sua vez devem ser "@Persistent" e a chave primária deve ser "@PrimaryKey".

3. Devemos criar uma classe PMF assim:

>	public final class PMF {
	 	private static final PersistenceManagerFactory
		pmfInstance = JDOHelper.getPersistenceManagerFactory("transactions-optional");
	 
		private PMF() {
		}

		public static PersistenceManagerFactory get() {
			return pmfInstance;
		}
	}

4. Devemos criar uma classe que extende HttpServlet que efetivamente irá enviar os dados para a nuvem usando o métodos doPost().

>	public class Upload extends HttpServlet {

		public void doPost(HttpServletRequest req, HttpServletResponse resp)
						throws IOException {
	
			String id = req.getParameter("id");
			String desc = req.getParameter("descricao");
			Descricao descricao = new Descricao(id, desc);

			PersistenceManager pm = PMF.get().getPersistenceManager();
			try {
				pm.makePersistent(descricao);
			} finally {
				pm.close();
			}
		}
	}

5. Devemos criar também a classe que fará o download dos dados da nuvem. Esta também deve extender a classe HttpServlet e deve implementar o método doGet().

>	public class Server extends HttpServlet {
	
		public void doGet(HttpServletRequest req, HttpServletResponse res) throws
				IOException {

			PersistenceManager pm =	PMF.get().getPersistenceManager();
			String id = req.getParameter("get");

			if(id.equals("all")){
				getAll(res, pm);
			} else {
				getId(res, pm, id);
			}
		}
	
		private void getAll(HttpServletResponse res, PersistenceManager pm) 
					throws IOException {
			Query query = pm.newQuery("select from " + Descricao.class.getName());
			List<Descricao> arquivos = null;
			try {
				arquivos = (List<Descricao>) query.execute();
			} finally {
				query.closeAll();
			}
			for (Descricao a : arquivos) {

				String id = a.getId();
				String desc = a.getDescricao();
	
				res.setContentType("text/html");
				res.setHeader("id", id);
				res.getWriter().println(id);
				res.setHeader("descricao", desc);
				res.getWriter().println(desc);

			}
		}
	
		private void getId(HttpServletResponse res, PersistenceManager pm, String reqId)
					throws IOException {
			Query query = pm.newQuery("select from " + Descricao.class.getName());
			List<Descricao> arquivos = null;
			try {
				arquivos = (List<Descricao>) query.execute();
			} finally {
				query.closeAll();
			}
			for (Descricao a : arquivos) {

				String desc = a.getDescricao();
				if (reqId.equals(a.getId())){

					res.setContentType("text/html");
					res.setHeader("descricao", desc);
					res.getWriter().println(desc);
				}
			}
		}
	}

6. Depois de escrever todos os códigos devemos adcionar no arquivo appengine-web.xml as informações sobre o App. Para isto precisamos modificar a linha, para conter o nome da aplicação: 

>	<application>trabfinalsd</application>

7. Devemos ainda colocar nossos servlets no arquivo web.xml seguindo o padrão:

>	<servlet>
	  <servlet-name>Upload</servlet-name>
	  <servlet-class>com.liriam.Upload</servlet-class>
	 </servlet>
	 <servlet-mapping>
	  <servlet-name>Upload</servlet-name>
	  <url-pattern>/upload</url-pattern>
	 </servlet-mapping>

8. Podemos e devemos fazer alguma interface para nossa aplicação na nuvem. Devemos fazer isto modificando o arquivo index.html. Este passo não é necessário para a aplicação funcionar, apenas facilita ao usar a aplicação diretamente do browser.

# Invocando a aplicação

9. Devemos criar um nojo projeto que conterá um cliente para nossa aplicação. Este cliente deve ter uma classe recipiente para receber os dados. E uma classe main que irá executar os métodos de receber e enviar os dados usando os servlets que implementam a interface da aplicação (Upload e Download).

>	public void enviarArquivo(Descricao desc) throws UnsupportedEncodingException{

		try {
			URL url = new URL("http://trabfinalsd.appspot.com/upload");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setRequestMethod("POST");
			OutputStreamWriter writer = new	OutputStreamWriter(connection.getOutputStream());
			writer.write("id=" + desc.getId() + "&descricao=" + desc.getDescricao());
			writer.close();
			if (connection.getResponseCode() ==	HttpURLConnection.HTTP_OK) {
			// OK
			} else {
			// Server returned HTTP error code.
			}
		} catch (MalformedURLException e) {
			// ...
		} catch (IOException e) {
			// ...
		}
	}

	public Descricao receberArquivo(String getId) throws UnsupportedEncodingException{
		Descricao desc = null;
		try {
			URL url = new URL("http://trabfinalsd.appspot.com/server?get=" + getId);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setRequestMethod("GET");
			String id;
			String descS;
			id = connection.getHeaderField("id");
			descS = connection.getHeaderField("descricao");
			desc = new Descricao(id, descS);
			if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
			} else {
				// Server returned HTTP error code.
			}
		} catch (MalformedURLException e) {
		// ...
		} catch (IOException e) {
		// ...
		}
		return desc;
	}

	public static void main(String[] args) throws IOException {
		Descricao desc = new Descricao(ID, DESC);
		ManipulacaoDeDados manip = new ManipulacaoDeDados();
		manip.enviarArquivo(desc);
		desc = manip.receberArquivo(id);
	}

