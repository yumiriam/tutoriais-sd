# Tutorial RMI

1. Criar três projetos no eclipse. Um projeto será o servidor, ele deve conter a implementação da interface RMI e o servidor. Outro será um projeto que conterá as coisas que são comuns tanto ao servidor quanto ai cliente, neste caso o FileBean e a interface RMI. E por um ultimo um cliente, que será apenas uma classe que chama o servidor.

# Criando a Interface

2. No projeto que deve contar as coisas comuns devemos criar a interface RMI, ela deve extender a classe Remote, e cada um de seus métodos deve jogar a exceção RemoteEsception. Devemos criar ainda neste projeto que será comum todos os artefatos que irão ser necessários para que exista uma comunicação entre cliente e servidor. No caso deste projeto precisaremos de uma classe que armazene os dados do arquivo e demos o nome de FileBean á ela.

3. Depois de terminar e compilar devemos transformar este projeto em um .jar que servirá de biblioteca tanto para o servidor quanto para o cliente. Este é um passo importante quando se implementa algo em RMI. Esse jar é essencial para que os dois lados consigam se conversar. Para criar um jar do projeto, basta dar um export nele para o formato.

# Criando o Servidor

4. No projeto do servidor devemos criar a implementação da interface criada acima, ou seja ela deve ter um "implements" Interface. Ela deve também extender a classe UnicastRemoteObject. Devemos implementar os métodos declarados na interface.

5. Criada a implementação da interface devemos agora criar o servidor propriamente dito. Deve ser uma classe com o método main. É nesta classe que devemos registrar o objeto(que é definido pela interface) no registro de nomes do RMI. Usamos as seguintes linhas de código para isso:

> Registry registry = LocateRegistry.getRegistry(PORT);
> registry.bind("InterfaceRMI", new InterfaceRMIImpl());

# Criando o Cliente

6. O cliente será uma classe simples com um método main que faz algum processamento de informação com a ajuda do servidor. O Cliente deve procurar no registro de nomes do RMI o objeto que ele deseja usar com as seguintes linhas de código:

> Registry registry = LocateRegistry.getRegistry(HOST, PORT);
> InterfaceRMI stub = (InterfaceRMI) registry.lookup("InterfaceRMI");

Feito isto ele pode usar o objeto remoto da mesma forma que um objeto local.

# Fazendo o código funcionar

7. Devemos seguir uns passos não tão simples para a execução do servidor. Devemos ligar o servidor de nomes do RMI, e em seguida invocar o servidor. Os comandos que usamos para isto foram:

> rmiregistry
> java -cp ../mongo-2.10.1.jar:../comum.jar:./bin/
>	-Djava.rmi.server.codebase=file:../comum.jar
>	-Djava.rmi.server.hostname=192.168.1.8
>	servidor.ServidorRMI

No segundo comando nós devemos passar como classpath (-cp) todos os .jar que serão necessários e o caminho dos executavéis já compilados. Feito isto devemos dizer para o RMI onde está o código base (codebase) que é tudo que o cliente precisa "baixar" (ou já ter invocado como biblioteca que foi o nosso caso) para conseguir conversar com o servidor. Precisamos ainda passar o local onde o servidor se encontra (neste caso usamos ip, mas pode ser resolvido por DNS também). E por fim o executavel do servidor.

8. O cliente deve ser executado normalmente.

# Links usados

http://littletutorials.com/2008/07/14/the-10-minutes-getting-started-with-rmi-tutorial
http://docs.oracle.com/javase/tutorial/rmi/running.html
http://docs.oracle.com/javase/6/docs/technotes/guides/rmi/hello/hello-world.html
http://docs.mongodb.org/ecosystem/tutorial/getting-started-with-java-driver

